#!/usr/bin/env bash
#####################
# FILE:    zfswrap.sh
# VERSION: 1.0-dev
# STATUS:  DEVELOPMENT
# AUTHOR:  steffen@fritz.wtf
# LICENSE: BSD-2-Clause <https://opensource.org/licenses/BSD-2-Clause>
#####################

set -e

##
# ANSI color  variables
##
red='\e[31m'
green='\e[32m'
blue='\e[34m'
clear='\e[0m'

##
# Color Functions
##
ColorRed(){
    echo -ne "$red""$1""$clear"
}
ColorGreen(){
    echo -ne "$green""$1""$clear"
}
ColorBlue(){
    echo -ne "$blue""$1""$clear"
}


##
# List pool info
##
function list_pool_info(){
    zpool status
}

##
# List snapshots
##
function list_snapshots(){
    zfs list -t snapshot
}

#
# List snapshot datasets
##
function list_snapshots_statistics(){
    echo "Snapshot Statistics"
    echo ""
    echo "Count Name" | column -t 
    zfs list -t snapshot | 
	    awk 'NR>2{ split($0,ss,"@"); print ss[1] }' | 
	    sort | 
	    uniq -c | 
	    sort -nr | column -t

    # ToDo iterate over list of datasets to compute storage used
    zfs get -Hp used
}


##
# List pool healths
##
function list_health(){
    zpool list -Ho name,health
}

##
# Get all pools
##
function get_pools(){
    zpool list -Ho name
}

##
# Purge last n snapshots
##
function purge_snapshots(){
    echo "ToDo"
}


##
# Add disk to pool
##
# ToDo

##
# Remove disk from pool
##
# ToDo



##
# Snapshot sub menu
##
snapshot_menu(){
echo -ne "
Snapshot submenu\n
$(ColorGreen '1)') List snapshot statistics 
$(ColorGreen '2)') List all snapshots
$(ColorGreen '3)') Purge snapshots
$(ColorGreen '0)') Back
$(ColorBlue 'Choose an option:') "
        read b
        case $b in
            1) list_snapshots_statistics ; snapshot_menu ;;
            2) list_snapshots ; snapshot_menu ;;
            3) purge_snapshots ; snapshot_menu ;;
            0) menu ;;
        esac
}


##
# Main menu
##
menu(){
echo -ne "
ZFS Management Console\n
$(ColorGreen '1)') Pool Overview
$(ColorGreen '2)') Health Overview
$(ColorGreen '3)') Snapshots
$(ColorGreen '0)') Exit
$(ColorBlue 'Choose an option:') "
        read a
        case $a in
        1) list_pool_info ; menu ;;
        2) list_health ; menu ;;
        3) snapshot_menu ; menu ;;
        0) exit 0 ;;
        *) echo -e "$red""Wrong option.""$clear"; WrongCommand;;
        esac
}

##
# menu() as entry point
##
menu
